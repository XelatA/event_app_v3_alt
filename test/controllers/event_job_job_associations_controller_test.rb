require 'test_helper'

class EventJobJobAssociationsControllerTest < ActionController::TestCase
  setup do
    @event_job_job_association = event_job_job_associations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:event_job_job_associations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create event_job_job_association" do
    assert_difference('EventJobJobAssociation.count') do
      post :create, event_job_job_association: { earliest_end: @event_job_job_association.earliest_end, earliest_start: @event_job_job_association.earliest_start, event_id: @event_job_job_association.event_id, latest_end: @event_job_job_association.latest_end, latest_start: @event_job_job_association.latest_start, predecessor_id: @event_job_job_association.predecessor_id, successor_id: @event_job_job_association.successor_id }
    end

    assert_redirected_to event_job_job_association_path(assigns(:event_job_job_association))
  end

  test "should show event_job_job_association" do
    get :show, id: @event_job_job_association
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @event_job_job_association
    assert_response :success
  end

  test "should update event_job_job_association" do
    patch :update, id: @event_job_job_association, event_job_job_association: { earliest_end: @event_job_job_association.earliest_end, earliest_start: @event_job_job_association.earliest_start, event_id: @event_job_job_association.event_id, latest_end: @event_job_job_association.latest_end, latest_start: @event_job_job_association.latest_start, predecessor_id: @event_job_job_association.predecessor_id, successor_id: @event_job_job_association.successor_id }
    assert_redirected_to event_job_job_association_path(assigns(:event_job_job_association))
  end

  test "should destroy event_job_job_association" do
    assert_difference('EventJobJobAssociation.count', -1) do
      delete :destroy, id: @event_job_job_association
    end

    assert_redirected_to event_job_job_associations_path
  end
end
