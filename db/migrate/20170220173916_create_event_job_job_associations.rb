class CreateEventJobJobAssociations < ActiveRecord::Migration
  def change
    create_table :event_job_job_associations do |t|
      t.integer :event_id
      t.integer :successor_id
      t.integer :predecessor_id
      t.integer :earliest_start
      t.integer :earliest_end
      t.integer :latest_start
      t.integer :latest_end

      t.timestamps null: false
    end
  end
end
