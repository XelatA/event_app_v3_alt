class CreateJobJobAssociations < ActiveRecord::Migration
  def change
    create_table :job_job_associations do |t|
      t.integer :predecessor_id
      t.integer :successor_id

      t.timestamps null: false
    end
  end
end
