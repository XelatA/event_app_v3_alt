Rails.application.routes.draw do

  resources :event_job_period_associations
  resources :event_job_job_associations
  resources :events
  resources :jobs
  resources :periods
  resources :job_job_associations
  resources :users
  resources :microposts,          only: [:create, :destroy]

  post 'periods/change', as: :change_periods
  get 'sessions/new'
  get 'users/new'
  root 'static_pages#home'

  get 'home'       => 'static_pages#home'
  get 'support'    => 'static_pages#support'
  get 'about'      => 'static_pages#about'
  get 'contact'    => 'static_pages#contact'
  get 'tcpsp'      => 'static_pages#tcpsp'
  get 'signup'     => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  post 'event_job_jobs/read_and_show_ofv', :to => 'event_job_job_associations#read_and_show_ofv'
  post 'event_job_jobs/read_optimization_results', :to => 'event_job_job_associations#read_optimization_results'
  post 'event_job_jobs/optimize', :to => 'event_job_job_associations#optimize'
  post 'event_job_jobs/delete_old_plan', :to => 'event_job_job_associations#delete_old_plan'
  ###post 'event_job_jobs/show_index_page', :to => 'event_job_job_associations#show_index_page'



  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

