class PeriodsController < ApplicationController
  before_action :set_period, only: [:show, :edit, :update, :destroy]

  # GET /periods
  # GET /periods.json
  def index
    @periods = Period.all
    @number_of_periods = Period.count
    if @mynumber_of_periods = nil
      @mynumber_of_periods=@number_of_periods
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @periods }
    end
  end


  def change
    @mynumber_of_periods = params[:mynewnumber].to_i
    @number_of_periods = Period.count
    number_of_missing_periods=@mynumber_of_periods - @number_of_periods

    if number_of_missing_periods > 0
      @events=Event.all
      @jobs=Job.all
      (1..number_of_missing_periods).each do |number|
        name="t#{number+@number_of_periods}"
        @new_period=Period.create!(name: name)
        @events.each { |eve|
          @jobs.each { |job|
            EventJobPeriodAssociation.create(event_id:eve.id, job_id: job.id, period_id: @new_period.id)
          }}
      end
    end

    if number_of_missing_periods < 0
      (@mynumber_of_periods +1 ..@number_of_periods).each do |number|
        name="t#{number}"
        Period.find_by_name(name).destroy
      end
    end

    @periods = Period.all
    render :template => "periods/index"
  end



  # GET /periods/1
  # GET /periods/1.json
  def show
  end

  # GET /periods/new
  def new
    @period = Period.new
  end

  # GET /periods/1/edit
  def edit
  end

  # POST /periods
  # POST /periods.json
  def create
    @period = Period.new(period_params)

    respond_to do |format|
      if @period.save
        format.html { redirect_to @period, notice: 'Period was successfully created.' }
        format.json { render :show, status: :created, location: @period }
      else
        format.html { render :new }
        format.json { render json: @period.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /periods/1
  # PATCH/PUT /periods/1.json
  def update
    respond_to do |format|
      if @period.update(period_params)
        format.html { redirect_to @period, notice: 'Period was successfully updated.' }
        format.json { render :show, status: :ok, location: @period }
      else
        format.html { render :edit }
        format.json { render json: @period.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /periods/1
  # DELETE /periods/1.json
 # def destroy
  #  @period.destroy
   # respond_to do |format|
   #   format.html { redirect_to periods_url, notice: 'Period was successfully destroyed.' }
   #   format.json { head :no_content }
    #  end
  #end



  private
  # Use callbacks to share common setup or constraints between actions.
  def set_period
    @period = Period.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def period_params
    params.require(:period).permit(:name, :add_cappa)
  end
end
