class EventJobJobAssociationsController < ApplicationController
  before_action :set_event_job_job_association, only: [:show, :edit, :update, :destroy]

  # GET /event_job_job_associations
  # GET /event_job_job_associations.json
  def index
    @event_job_job_associations = EventJobJobAssociation.all
  end

  # GET /event_job_job_associations/1
  # GET /event_job_job_associations/1.json
  def show
  end

  # GET /event_job_job_associations/new
  def new
    @event_job_job_association = EventJobJobAssociation.new
  end

  # GET /event_job_job_associations/1/edit
  def edit
  end

  # POST /event_job_job_associations
  # POST /event_job_job_associations.json
  def create
    @event_job_job_association = EventJobJobAssociation.new(event_job_job_association_params)

    respond_to do |format|
      if @event_job_job_association.save
        format.html { redirect_to @event_job_job_association, notice: 'Event job job association was successfully created.' }
        format.json { render :show, status: :created, location: @event_job_job_association }
      else
        format.html { render :new }
        format.json { render json: @event_job_job_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /event_job_job_associations/1
  # PATCH/PUT /event_job_job_associations/1.json
  def update
    respond_to do |format|
      if @event_job_job_association.update(event_job_job_association_params)
        format.html { redirect_to @event_job_job_association, notice: 'Event job job association was successfully updated.' }
        format.json { render :show, status: :ok, location: @event_job_job_association }
      else
        format.html { render :edit }
        format.json { render json: @event_job_job_association.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /event_job_job_associations/1
  # DELETE /event_job_job_associations/1.json
  def destroy
    @event_job_job_association.destroy
    respond_to do |format|
      format.html { redirect_to event_job_job_associations_url, notice: 'Event job job association was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def optimize

    if File.exist?("Multi_Projekt_Input.inc")
      File.delete("Multi_Projekt_Input.inc")
    end
    f=File.new("Multi_Projekt_Input.inc", "w")

    printf(f, "set p / \n")
    @events = Event.all
    @events.each { |event| printf(f, event.name + "\n") }
    printf(f, "/" + "\n\n")

    printf(f, "set i / \n")
    @jobs = Job.all
    @jobs.each { |job| printf(f, job.name + "\n") }
    printf(f, "/" + "\n\n")

    printf(f, "set t / \n")
    @periods = Period.all
    @periods.each { |per| printf(f, per.name + "\n") }
    printf(f, "/;\n\n")

    printf(f, "set pit / \n")
    @event_job_period_associations = EventJobPeriodAssociation.all
    @event_job_period_associations.each { |eve_job_per| printf(f, "pit" + eve_job_per.id.to_s + "\n") }
    printf(f, "/;" + "\n\n")

    printf(f, "PITpit(pit,p,i,t)=no;" + "\n\n")
    @event_job_period_associations.each { |eve_job_per| printf(f, "PITpit('pit"+ eve_job_per.id.to_s + "','" + eve_job_per.event_id.to_s + "','" + eve_job_per.job_id.to_s + "','" + eve_job_per.period_id.to_s + "')=yes ; \n")
    }
    printf(f, "\n\n")


    printf(f, "VN(h,i)=no;" + "\n\n")
    @job_job_associations = JobJobAssociation.all
    @job_job_associations.each { |job_job|
      printf(f, "VN('" + job_job.predecessor.name+"','" + job_job.successor.name+"')=yes;\n")
      printf(f, "\n\n")
    }

    ### Start- und Endzeitpunkte

    printf(f, "set pih / \n")
    @event_job_job_associations=EventJobJobAssociation.all
    @event_job_job_associations.each { |eve_jojo| printf(f, "pih" + eve_jojo.id.to_s + "\n") }
    printf(f, "/;" + "\n\n")

    printf(f, "PIHpih(pih,p,i,h)=no;" + "\n\n")
    @event_job_job_associations.each { |eve_jojo| printf(f, "PIHpih('pih"+ eve_jojo.id.to_s + "','" + eve_jojo.event_id.to_s + "','" + eve_jojo.successor_id.to_s + "','" + eve_jojo.predecessor_id.to_s + "')=yes ; \n")
    }
    printf(f, "\n\n")

    printf(f, "\n")
    @events.each { |eve|
      printf(f, "Deadline('" + eve.name + "')="+ eve.Deadline.to_s + ";\n")
    }

    printf(f, "\n")

    @jobs.each { |job|
      printf(f, "k('" + job.name + "')="+ job.ressource_demand.to_s + ";\n")
    }

    printf(f, "\n")

    printf(f, "oc = 800 ;\n")

    printf(f, "\n")

    f.close


    if File.exist?("TCPSP_solution.txt")
      File.delete("TCPSP_solution.txt")
    end

    system "C:\\GAMS\\win64\\24.7\\gams Multi_project_RCPSP.gms"

    flash.now[:started] = "Die Rechnung wurde gestartet!"

    render 'static_pages/tcpsp'


  end


  def delete_old_plan

    if File.exist?("TCPSP_solution_PIH.txt")
      File.delete("TCPSP_solution_PIH.txt")
    end

    if File.exist?("TCPSP_solution_add_cappa.txt")
      File.delete("TCPSP_solution_add_cappa.txt")
    end

    if File.exist?("TCPSP_solution_PIT.txt")
      File.delete("TCPSP_solution_PIT.txt")
    end

    if File.exist?("TCPSP_Zfkt.txt")
      File.delete("TCPSP_Zfkt.txt")
    end

    @periods=Period.all
    @periods.each { |peri|
      peri.add_cappa=nil
      peri.save
    }

    @event_job_job_associations = EventJobJobAssociation.all
    @event_job_job_associations.each { |eve_jojo|
      eve_jojo.earliest_start=nil
      eve_jojo.earliest_end=nil
      eve_jojo.latest_start=nil
      eve_jojo.latest_end=nil
      eve_jojo.save
    }

    @event_job_period_associations=EventJobPeriodAssociation.all
    @event_job_period_associations.each { |eve_job_peri|
      eve_job_peri.job_end=nil
    }

    @objective_function_value=nil

    render :template => "periods/index"

  end


  def read_optimization_results

    if File.exist?("TCPSP_solution_add_cappa.txt")
      fi=File.open("TCPSP_solution_add_cappa.txt", "r")
      fi.each { |line|
        sa=line.split(";")
        sa0=sa[0].delete "add_cappa"
        sa1=sa[1].delete " \n"
        period=Period.find_by_id(sa0)
        period.add_cappa = sa1
        period.save
      }
      fi.close

      if File.exists?("TCPSP_solution_PIT.txt")
        fi=File.open("TCPSP_solution_PIT.txt", "r")
        fi.each { |line|
          sa=line.split(";")
          sa0=sa[0].delete "pit"
          sa4=sa[4].delete " \n"
          event_job_period_association=EventJobPeriodAssociation.find_by_id(sa0)
          event_job_period_association.job_end = sa4
          product_period_association.save
        }
        fi.close

        if File.exists?("TCPSP_solution_PIH.txt")
          fi=File.open("TCPSP_solution_PIH.txt", "r")
          fi.each { |line|
            sa=line.split(";")
            sa0=sa[0].delete "pih"
            sa4=sa[4]
            sa5=sa[5]
            sa6=sa[6]
            sa7=sa[7].delete " \n"
            event_job_job_association=EventJobJobAssociation.find_by_id(sa0)
            event_job_job_association.earliest_start = sa4
            event_job_job_association.earliest_end = sa5
            event_job_job_association.latest_start = sa6
            event_job_job_association.latest_end = sa7
            event_job_job_association.save
          }
          fi.close
        else
          flash.now[:not_available] = "Die Lösung wurde noch nicht berechnet!"
        end
      end
    end
    @event_job_period_associations = EventJobPeriodAssociation.all
    render :template => "event_job_period_associations/index"

  end


  def read_and_show_ofv

    if File.exist?("TCPSP_Zfkt.txt")
      fi=File.open("TCPSP_Zfkt.txt", "r")
      line=fi.readline
      fi.close
      sa=line.split(" ")
      @objective_function_value=sa[1]
    else
      @objective_function_value=nil
      flash.now[:not_available] = "Zielfunktionswert wurde noch nicht berechnet!"

    end

    @periods=Period.all
    render :template => "periods/index"
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event_job_job_association
      @event_job_job_association = EventJobJobAssociation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_job_job_association_params
      params.require(:event_job_job_association).permit(:event_id, :successor_id, :predecessor_id, :earliest_start, :earliest_end, :latest_start, :latest_end)
    end
end
