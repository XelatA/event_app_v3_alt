class Job < ActiveRecord::Base
  has_many :job_job_associations, foreign_key: "successor_id", :dependent => :destroy
  has_many :reverse_job_associations, foreign_key: "predecessor_id", class_name: "JobJobAssociation", :dependent => :destroy
  has_many :successors, through: :job_job_associations, source: :successor_id
  has_many :predecessors, through: :reverse_job_associations, source: :predecessor_id

  has_many :events, through: :event_job_period_associations
  has_many :periods, through: :event_job_period_associations

  has_many :event_job_job_associations, foreign_key: "successor_id", :dependent => :destroy
  has_many :event_reverse_job_associations, foreign_key: "predecessor_id", class_name: "EventJobJobAssociation", :dependent => :destroy
  has_many :successors, through: :event_job_job_associations, source: :successor_id
  has_many :predecessors, through: :event_reverse_job_associations, source: :predecessor_id
end
