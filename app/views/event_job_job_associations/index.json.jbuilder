json.array!(@event_job_job_associations) do |event_job_job_association|
  json.extract! event_job_job_association, :id, :event_id, :successor_id, :predecessor_id=integer, :earliest_start, :earliest_end, :latest_start, :latest_end
  json.url event_job_job_association_url(event_job_job_association, format: :json)
end
