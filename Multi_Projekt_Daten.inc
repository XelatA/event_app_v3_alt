set p /p1, p2, p3/;

set i / i1*i7/;

* Achtung: Erste Periode/erster Zeitpunkt muss 0 sein

set t /t0*t15/;

VN(h,i)=no;

* Achtung: Topologische Sortierung erforderlich

VN('i1','i2')=yes;
VN('i2','i3')=yes;
VN('i3','i4')=yes;
VN('i3','i5')=yes;
VN('i4','i6')=yes;
VN('i5','i6')=yes;
VN('i6','i7')=yes;;

parameter
        d(i) /
         i1      0
         i2      3
         i3      1
         i4      1
         i5      1
         i6      2
         i7      0/;


parameter
Deadline(p) /
p1       7
p2       14
p3       12/;


k(i)=0;

k('i1')=0;
k('i2')=3;
k('i3')=1;
k('i4')=1;
k('i5')=1;
k('i6')=3;
k('i7')=0;

KP=4;

oc=800;



set pih /pih1*pih21/;

PIHpih(pih,p,i,h)=no;

PIHpih('pih1','p1','i1','i2')=yes;
PIHpih('pih2','p1','i2','i3')=yes;
PIHpih('pih3','p1','i3','i4')=yes;
PIHpih('pih4','p1','i3','i5')=yes;
PIHpih('pih5','p1','i4','i6')=yes;
PIHpih('pih6','p1','i5','i6')=yes;
PIHpih('pih7','p1','i6','i7')=yes;
PIHpih('pih8','p2','i1','i2')=yes;
PIHpih('pih9','p2','i2','i3')=yes;
PIHpih('pih10','p2','i3','i4')=yes;
PIHpih('pih11','p2','i3','i5')=yes;
PIHpih('pih12','p2','i4','i6')=yes;
PIHpih('pih13','p2','i5','i6')=yes;
PIHpih('pih14','p2','i6','i7')=yes;
PIHpih('pih15','p3','i1','i2')=yes;
PIHpih('pih16','p3','i2','i3')=yes;
PIHpih('pih17','p3','i3','i4')=yes;
PIHpih('pih18','p3','i3','i5')=yes;
PIHpih('pih19','p3','i4','i6')=yes;
PIHpih('pih20','p3','i5','i6')=yes;
PIHpih('pih21','p3','i6','i7')=yes;




set pit /pit1*pit384/;

PITpit(pit,p,i,t)=no;

PITpit('pit1','p1','i1','t0')=yes;
PITpit('pit2','p1','i1','t1')=yes;
PITpit('pit3','p1','i1','t2')=yes;
PITpit('pit4','p1','i1','t3')=yes;
PITpit('pit5','p1','i1','t4')=yes;
PITpit('pit6','p1','i1','t5')=yes;
PITpit('pit7','p1','i1','t6')=yes;
PITpit('pit8','p1','i1','t7')=yes;
PITpit('pit9','p1','i1','t8')=yes;
PITpit('pit10','p1','i1','t9')=yes;
PITpit('pit11','p1','i1','t10')=yes;
PITpit('pit12','p1','i1','t11')=yes;
PITpit('pit13','p1','i1','t12')=yes;
PITpit('pit14','p1','i1','t13')=yes;
PITpit('pit15','p1','i1','t14')=yes;
PITpit('pit16','p1','i1','t15')=yes;
PITpit('pit17','p1','i2','t0')=yes;
PITpit('pit18','p1','i2','t1')=yes;
PITpit('pit19','p1','i2','t2')=yes;
PITpit('pit20','p1','i2','t3')=yes;
PITpit('pit21','p1','i2','t4')=yes;
PITpit('pit22','p1','i2','t5')=yes;
PITpit('pit23','p1','i2','t6')=yes;
PITpit('pit24','p1','i2','t7')=yes;
PITpit('pit25','p1','i2','t8')=yes;
PITpit('pit26','p1','i2','t9')=yes;
PITpit('pit27','p1','i2','t10')=yes;
PITpit('pit28','p1','i2','t11')=yes;
PITpit('pit29','p1','i2','t12')=yes;
PITpit('pit30','p1','i2','t13')=yes;
PITpit('pit31','p1','i2','t14')=yes;
PITpit('pit32','p1','i2','t15')=yes;
PITpit('pit33','p1','i3','t0')=yes;
PITpit('pit34','p1','i3','t1')=yes;
PITpit('pit35','p1','i3','t2')=yes;
PITpit('pit36','p1','i3','t3')=yes;
PITpit('pit37','p1','i3','t4')=yes;
PITpit('pit38','p1','i3','t5')=yes;
PITpit('pit39','p1','i3','t6')=yes;
PITpit('pit40','p1','i3','t7')=yes;
PITpit('pit41','p1','i3','t8')=yes;
PITpit('pit42','p1','i3','t9')=yes;
PITpit('pit43','p1','i3','t10')=yes;
PITpit('pit44','p1','i3','t11')=yes;
PITpit('pit45','p1','i3','t12')=yes;
PITpit('pit46','p1','i3','t13')=yes;
PITpit('pit47','p1','i3','t14')=yes;
PITpit('pit48','p1','i3','t15')=yes;
PITpit('pit49','p1','i4','t0')=yes;
PITpit('pit50','p1','i4','t1')=yes;
PITpit('pit51','p1','i4','t2')=yes;
PITpit('pit52','p1','i4','t3')=yes;
PITpit('pit53','p1','i4','t4')=yes;
PITpit('pit54','p1','i4','t5')=yes;
PITpit('pit55','p1','i4','t6')=yes;
PITpit('pit56','p1','i4','t7')=yes;
PITpit('pit57','p1','i4','t8')=yes;
PITpit('pit58','p1','i4','t9')=yes;
PITpit('pit59','p1','i4','t10')=yes;
PITpit('pit60','p1','i4','t11')=yes;
PITpit('pit61','p1','i4','t12')=yes;
PITpit('pit62','p1','i4','t13')=yes;
PITpit('pit63','p1','i4','t14')=yes;
PITpit('pit64','p1','i4','t15')=yes;
PITpit('pit65','p1','i5','t0')=yes;
PITpit('pit66','p1','i5','t1')=yes;
PITpit('pit67','p1','i5','t2')=yes;
PITpit('pit68','p1','i5','t3')=yes;
PITpit('pit69','p1','i5','t4')=yes;
PITpit('pit70','p1','i5','t5')=yes;
PITpit('pit71','p1','i5','t6')=yes;
PITpit('pit72','p1','i5','t7')=yes;
PITpit('pit73','p1','i5','t8')=yes;
PITpit('pit74','p1','i5','t9')=yes;
PITpit('pit75','p1','i5','t10')=yes;
PITpit('pit76','p1','i5','t11')=yes;
PITpit('pit77','p1','i5','t12')=yes;
PITpit('pit78','p1','i5','t13')=yes;
PITpit('pit79','p1','i5','t14')=yes;
PITpit('pit80','p1','i5','t15')=yes;
PITpit('pit81','p1','i5','t0')=yes;
PITpit('pit82','p1','i5','t1')=yes;
PITpit('pit83','p1','i5','t2')=yes;
PITpit('pit84','p1','i5','t3')=yes;
PITpit('pit85','p1','i5','t4')=yes;
PITpit('pit86','p1','i5','t5')=yes;
PITpit('pit87','p1','i5','t6')=yes;
PITpit('pit88','p1','i5','t7')=yes;
PITpit('pit89','p1','i5','t8')=yes;
PITpit('pit90','p1','i5','t9')=yes;
PITpit('pit91','p1','i5','t10')=yes;
PITpit('pit92','p1','i5','t11')=yes;
PITpit('pit93','p1','i5','t12')=yes;
PITpit('pit94','p1','i5','t13')=yes;
PITpit('pit95','p1','i5','t14')=yes;
PITpit('pit96','p1','i5','t15')=yes;
PITpit('pit97','p1','i6','t0')=yes;
PITpit('pit98','p1','i6','t1')=yes;
PITpit('pit99','p1','i6','t2')=yes;
PITpit('pit100','p1','i6','t3')=yes;
PITpit('pit101','p1','i6','t4')=yes;
PITpit('pit102','p1','i6','t5')=yes;
PITpit('pit103','p1','i6','t6')=yes;
PITpit('pit104','p1','i6','t7')=yes;
PITpit('pit105','p1','i6','t8')=yes;
PITpit('pit106','p1','i6','t9')=yes;
PITpit('pit107','p1','i6','t10')=yes;
PITpit('pit108','p1','i6','t11')=yes;
PITpit('pit109','p1','i6','t12')=yes;
PITpit('pit110','p1','i6','t13')=yes;
PITpit('pit111','p1','i6','t14')=yes;
PITpit('pit112','p1','i6','t15')=yes;
PITpit('pit113','p1','i7','t0')=yes;
PITpit('pit114','p1','i7','t1')=yes;
PITpit('pit115','p1','i7','t2')=yes;
PITpit('pit116','p1','i7','t3')=yes;
PITpit('pit117','p1','i7','t4')=yes;
PITpit('pit118','p1','i7','t5')=yes;
PITpit('pit119','p1','i7','t6')=yes;
PITpit('pit120','p1','i7','t7')=yes;
PITpit('pit121','p1','i7','t8')=yes;
PITpit('pit122','p1','i7','t9')=yes;
PITpit('pit123','p1','i7','t10')=yes;
PITpit('pit124','p1','i7','t11')=yes;
PITpit('pit125','p1','i7','t12')=yes;
PITpit('pit126','p1','i7','t13')=yes;
PITpit('pit127','p1','i7','t14')=yes;
PITpit('pit128','p1','i7','t15')=yes;
PITpit('pit129','p2','i1','t0')=yes;
PITpit('pit130','p2','i1','t1')=yes;
PITpit('pit131','p2','i1','t2')=yes;
PITpit('pit132','p2','i1','t3')=yes;
PITpit('pit133','p2','i1','t4')=yes;
PITpit('pit134','p2','i1','t5')=yes;
PITpit('pit135','p2','i1','t6')=yes;
PITpit('pit136','p2','i1','t7')=yes;
PITpit('pit137','p2','i1','t8')=yes;
PITpit('pit138','p2','i1','t9')=yes;
PITpit('pit139','p2','i1','t10')=yes;
PITpit('pit140','p2','i1','t11')=yes;
PITpit('pit141','p2','i1','t12')=yes;
PITpit('pit142','p2','i1','t13')=yes;
PITpit('pit143','p2','i1','t14')=yes;
PITpit('pit144','p2','i1','t15')=yes;
PITpit('pit145','p2','i2','t0')=yes;
PITpit('pit146','p2','i2','t1')=yes;
PITpit('pit147','p2','i2','t2')=yes;
PITpit('pit148','p2','i2','t3')=yes;
PITpit('pit149','p2','i2','t4')=yes;
PITpit('pit150','p2','i2','t5')=yes;
PITpit('pit151','p2','i2','t6')=yes;
PITpit('pit152','p2','i2','t7')=yes;
PITpit('pit153','p2','i2','t8')=yes;
PITpit('pit154','p2','i2','t9')=yes;
PITpit('pit155','p2','i2','t10')=yes;
PITpit('pit156','p2','i2','t11')=yes;
PITpit('pit157','p2','i2','t12')=yes;
PITpit('pit158','p2','i2','t13')=yes;
PITpit('pit159','p2','i2','t14')=yes;
PITpit('pit160','p2','i2','t15')=yes;
PITpit('pit161','p2','i3','t0')=yes;
PITpit('pit162','p2','i3','t1')=yes;
PITpit('pit163','p2','i3','t2')=yes;
PITpit('pit164','p2','i3','t3')=yes;
PITpit('pit165','p2','i3','t4')=yes;
PITpit('pit166','p2','i3','t5')=yes;
PITpit('pit167','p2','i3','t6')=yes;
PITpit('pit168','p2','i3','t7')=yes;
PITpit('pit169','p2','i3','t8')=yes;
PITpit('pit170','p2','i3','t9')=yes;
PITpit('pit171','p2','i3','t10')=yes;
PITpit('pit172','p2','i3','t11')=yes;
PITpit('pit173','p2','i3','t12')=yes;
PITpit('pit174','p2','i3','t13')=yes;
PITpit('pit175','p2','i3','t14')=yes;
PITpit('pit176','p2','i3','t15')=yes;
PITpit('pit177','p2','i4','t0')=yes;
PITpit('pit178','p2','i4','t1')=yes;
PITpit('pit179','p2','i4','t2')=yes;
PITpit('pit180','p2','i4','t3')=yes;
PITpit('pit181','p2','i4','t4')=yes;
PITpit('pit182','p2','i4','t5')=yes;
PITpit('pit183','p2','i4','t6')=yes;
PITpit('pit184','p2','i4','t7')=yes;
PITpit('pit185','p2','i4','t8')=yes;
PITpit('pit186','p2','i4','t9')=yes;
PITpit('pit187','p2','i4','t10')=yes;
PITpit('pit188','p2','i4','t11')=yes;
PITpit('pit189','p2','i4','t12')=yes;
PITpit('pit190','p2','i4','t13')=yes;
PITpit('pit191','p2','i4','t14')=yes;
PITpit('pit192','p2','i4','t15')=yes;
PITpit('pit193','p2','i5','t0')=yes;
PITpit('pit194','p2','i5','t1')=yes;
PITpit('pit195','p2','i5','t2')=yes;
PITpit('pit196','p2','i5','t3')=yes;
PITpit('pit197','p2','i5','t4')=yes;
PITpit('pit198','p2','i5','t5')=yes;
PITpit('pit199','p2','i5','t6')=yes;
PITpit('pit200','p2','i5','t7')=yes;
PITpit('pit201','p2','i5','t8')=yes;
PITpit('pit202','p2','i5','t9')=yes;
PITpit('pit203','p2','i5','t10')=yes;
PITpit('pit204','p2','i5','t11')=yes;
PITpit('pit205','p2','i5','t12')=yes;
PITpit('pit206','p2','i5','t13')=yes;
PITpit('pit207','p2','i5','t14')=yes;
PITpit('pit208','p2','i5','t15')=yes;
PITpit('pit209','p2','i5','t0')=yes;
PITpit('pit210','p2','i5','t1')=yes;
PITpit('pit211','p2','i5','t2')=yes;
PITpit('pit212','p2','i5','t3')=yes;
PITpit('pit213','p2','i5','t4')=yes;
PITpit('pit214','p2','i5','t5')=yes;
PITpit('pit215','p2','i5','t6')=yes;
PITpit('pit216','p2','i5','t7')=yes;
PITpit('pit217','p2','i5','t8')=yes;
PITpit('pit218','p2','i5','t9')=yes;
PITpit('pit219','p2','i5','t10')=yes;
PITpit('pit220','p2','i5','t11')=yes;
PITpit('pit221','p2','i5','t12')=yes;
PITpit('pit222','p2','i5','t13')=yes;
PITpit('pit223','p2','i5','t14')=yes;
PITpit('pit224','p2','i5','t15')=yes;
PITpit('pit225','p2','i6','t0')=yes;
PITpit('pit226','p2','i6','t1')=yes;
PITpit('pit227','p2','i6','t2')=yes;
PITpit('pit228','p2','i6','t3')=yes;
PITpit('pit229','p2','i6','t4')=yes;
PITpit('pit230','p2','i6','t5')=yes;
PITpit('pit231','p2','i6','t6')=yes;
PITpit('pit232','p2','i6','t7')=yes;
PITpit('pit233','p2','i6','t8')=yes;
PITpit('pit234','p2','i6','t9')=yes;
PITpit('pit235','p2','i6','t10')=yes;
PITpit('pit236','p2','i6','t11')=yes;
PITpit('pit237','p2','i6','t12')=yes;
PITpit('pit238','p2','i6','t13')=yes;
PITpit('pit239','p2','i6','t14')=yes;
PITpit('pit240','p2','i6','t15')=yes;
PITpit('pit241','p2','i7','t0')=yes;
PITpit('pit242','p2','i7','t1')=yes;
PITpit('pit243','p2','i7','t2')=yes;
PITpit('pit244','p2','i7','t3')=yes;
PITpit('pit245','p2','i7','t4')=yes;
PITpit('pit246','p2','i7','t5')=yes;
PITpit('pit247','p2','i7','t6')=yes;
PITpit('pit248','p2','i7','t7')=yes;
PITpit('pit249','p2','i7','t8')=yes;
PITpit('pit250','p2','i7','t9')=yes;
PITpit('pit251','p2','i7','t10')=yes;
PITpit('pit252','p2','i7','t11')=yes;
PITpit('pit253','p2','i7','t12')=yes;
PITpit('pit254','p2','i7','t13')=yes;
PITpit('pit255','p2','i7','t14')=yes;
PITpit('pit256','p2','i7','t15')=yes;
PITpit('pit257','p3','i1','t0')=yes;
PITpit('pit258','p3','i1','t1')=yes;
PITpit('pit259','p3','i1','t2')=yes;
PITpit('pit260','p3','i1','t3')=yes;
PITpit('pit261','p3','i1','t4')=yes;
PITpit('pit262','p3','i1','t5')=yes;
PITpit('pit263','p3','i1','t6')=yes;
PITpit('pit264','p3','i1','t7')=yes;
PITpit('pit265','p3','i1','t8')=yes;
PITpit('pit266','p3','i1','t9')=yes;
PITpit('pit267','p3','i1','t10')=yes;
PITpit('pit268','p3','i1','t11')=yes;
PITpit('pit269','p3','i1','t12')=yes;
PITpit('pit270','p3','i1','t13')=yes;
PITpit('pit271','p3','i1','t14')=yes;
PITpit('pit272','p3','i1','t15')=yes;
PITpit('pit273','p3','i2','t0')=yes;
PITpit('pit274','p3','i2','t1')=yes;
PITpit('pit275','p3','i2','t2')=yes;
PITpit('pit276','p3','i2','t3')=yes;
PITpit('pit277','p3','i2','t4')=yes;
PITpit('pit278','p3','i2','t5')=yes;
PITpit('pit279','p3','i2','t6')=yes;
PITpit('pit280','p3','i2','t7')=yes;
PITpit('pit281','p3','i2','t8')=yes;
PITpit('pit282','p3','i2','t9')=yes;
PITpit('pit283','p3','i2','t10')=yes;
PITpit('pit284','p3','i2','t11')=yes;
PITpit('pit285','p3','i2','t12')=yes;
PITpit('pit286','p3','i2','t13')=yes;
PITpit('pit287','p3','i2','t14')=yes;
PITpit('pit288','p3','i2','t15')=yes;
PITpit('pit289','p3','i3','t0')=yes;
PITpit('pit290','p3','i3','t1')=yes;
PITpit('pit291','p3','i3','t2')=yes;
PITpit('pit292','p3','i3','t3')=yes;
PITpit('pit293','p3','i3','t4')=yes;
PITpit('pit294','p3','i3','t5')=yes;
PITpit('pit295','p3','i3','t6')=yes;
PITpit('pit296','p3','i3','t7')=yes;
PITpit('pit297','p3','i3','t8')=yes;
PITpit('pit298','p3','i3','t9')=yes;
PITpit('pit299','p3','i3','t10')=yes;
PITpit('pit300','p3','i3','t11')=yes;
PITpit('pit301','p3','i3','t12')=yes;
PITpit('pit302','p3','i3','t13')=yes;
PITpit('pit303','p3','i3','t14')=yes;
PITpit('pit304','p3','i3','t15')=yes;
PITpit('pit305','p3','i4','t0')=yes;
PITpit('pit306','p3','i4','t1')=yes;
PITpit('pit307','p3','i4','t2')=yes;
PITpit('pit308','p3','i4','t3')=yes;
PITpit('pit309','p3','i4','t4')=yes;
PITpit('pit310','p3','i4','t5')=yes;
PITpit('pit311','p3','i4','t6')=yes;
PITpit('pit312','p3','i4','t7')=yes;
PITpit('pit313','p3','i4','t8')=yes;
PITpit('pit314','p3','i4','t9')=yes;
PITpit('pit315','p3','i4','t10')=yes;
PITpit('pit316','p3','i4','t11')=yes;
PITpit('pit317','p3','i4','t12')=yes;
PITpit('pit318','p3','i4','t13')=yes;
PITpit('pit319','p3','i4','t14')=yes;
PITpit('pit320','p3','i4','t15')=yes;
PITpit('pit321','p3','i5','t0')=yes;
PITpit('pit322','p3','i5','t1')=yes;
PITpit('pit323','p3','i5','t2')=yes;
PITpit('pit324','p3','i5','t3')=yes;
PITpit('pit325','p3','i5','t4')=yes;
PITpit('pit326','p3','i5','t5')=yes;
PITpit('pit327','p3','i5','t6')=yes;
PITpit('pit328','p3','i5','t7')=yes;
PITpit('pit329','p3','i5','t8')=yes;
PITpit('pit330','p3','i5','t9')=yes;
PITpit('pit331','p3','i5','t10')=yes;
PITpit('pit332','p3','i5','t11')=yes;
PITpit('pit333','p3','i5','t12')=yes;
PITpit('pit334','p3','i5','t13')=yes;
PITpit('pit335','p3','i5','t14')=yes;
PITpit('pit336','p3','i5','t15')=yes;
PITpit('pit337','p3','i5','t0')=yes;
PITpit('pit338','p3','i5','t1')=yes;
PITpit('pit339','p3','i5','t2')=yes;
PITpit('pit340','p3','i5','t3')=yes;
PITpit('pit341','p3','i5','t4')=yes;
PITpit('pit342','p3','i5','t5')=yes;
PITpit('pit343','p3','i5','t6')=yes;
PITpit('pit344','p3','i5','t7')=yes;
PITpit('pit345','p3','i5','t8')=yes;
PITpit('pit346','p3','i5','t9')=yes;
PITpit('pit347','p3','i5','t10')=yes;
PITpit('pit348','p3','i5','t11')=yes;
PITpit('pit349','p3','i5','t12')=yes;
PITpit('pit350','p3','i5','t13')=yes;
PITpit('pit351','p3','i5','t14')=yes;
PITpit('pit352','p3','i5','t15')=yes;
PITpit('pit353','p3','i6','t0')=yes;
PITpit('pit354','p3','i6','t1')=yes;
PITpit('pit355','p3','i6','t2')=yes;
PITpit('pit356','p3','i6','t3')=yes;
PITpit('pit357','p3','i6','t4')=yes;
PITpit('pit358','p3','i6','t5')=yes;
PITpit('pit359','p3','i6','t6')=yes;
PITpit('pit360','p3','i6','t7')=yes;
PITpit('pit361','p3','i6','t8')=yes;
PITpit('pit362','p3','i6','t9')=yes;
PITpit('pit363','p3','i6','t10')=yes;
PITpit('pit364','p3','i6','t11')=yes;
PITpit('pit365','p3','i6','t12')=yes;
PITpit('pit366','p3','i6','t13')=yes;
PITpit('pit367','p3','i6','t14')=yes;
PITpit('pit368','p3','i6','t15')=yes;
PITpit('pit369','p3','i7','t0')=yes;
PITpit('pit370','p3','i7','t1')=yes;
PITpit('pit371','p3','i7','t2')=yes;
PITpit('pit372','p3','i7','t3')=yes;
PITpit('pit373','p3','i7','t4')=yes;
PITpit('pit374','p3','i7','t5')=yes;
PITpit('pit375','p3','i7','t6')=yes;
PITpit('pit376','p3','i7','t7')=yes;
PITpit('pit377','p3','i7','t8')=yes;
PITpit('pit378','p3','i7','t9')=yes;
PITpit('pit379','p3','i7','t10')=yes;
PITpit('pit380','p3','i7','t11')=yes;
PITpit('pit381','p3','i7','t12')=yes;
PITpit('pit382','p3','i7','t13')=yes;
PITpit('pit383','p3','i7','t14')=yes;
PITpit('pit384','p3','i7','t15')=yes;
